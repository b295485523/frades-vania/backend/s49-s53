const User = require("../models/User")
const Product = require("../models/Product");
const Order = require('../models/Order');


// [ ADD Product ] Admin only
    module.exports.addProduct = (req, res) => {

        let newProduct = new Product({
            name: req.body.name,
            description: req.body.description,
            price: req.body.price
        })

        return newProduct.save().then((product) => {

            return res.send(product);
        }).catch(err => res.send(err));


    };

// [ RETRIEVE All Active Products ]
    module.exports.getAllActive = (req, res) => {

      Product.find({ isActive: true }).then(product => {

          return res.send(product);

        }).catch(err => res.send(err))
    };
    
// [ RETRIEVE Specific Product ]
    module.exports.getSpecificProduct = (req, res) => {

        return Product.findById(req.params.productId).then(result => {

            return res.send(result)
        })
        .catch(err => res.send(err))
    };

// [ RETRIEVE All Products ]
    module.exports.getAllProducts = (req, res) => {

      Product.find({}).then(products => {

          res.send(products);

        }).catch(err => res.send(err))
    };

// [ UPDATE Product Info ] Admin only
    module.exports.updateProduct = (req, res) => {

        let updatedProduct = {

            name: req.body.name,
            category: req.body.category,
            description: req.body.description,
            price: req.body.price
        }

        return Product.findByIdAndUpdate(req.params.productId, updatedProduct) 
        .then((product, error) => {

            if(error) {
                return res.send(false);
            } else {
                return res.send(true);
            }
        }).catch(err => res.send(err))
    };

// [ ARCHIVE Product ] Admin only
    module.exports.archiveProduct = (req, res) => {

        let archivedProduct = {
            isActive: false
        }
        return Product.findByIdAndUpdate(req.params.productId, archivedProduct)
        .then((product, error) => {

            if(error) {
                return res.send(false);
            } else {
                return res.send(true);
            }

        }).catch(err => res.send(err))
    };

// [ ACTIVATE Product ] Admin only
    module.exports.activateProduct = (req, res) => {

        let activatedProduct = {
            isActive: true
        }

        return Product.findByIdAndUpdate(req.params.productId, activatedProduct)
        .then((product, error) => {

            if(error) {
                return res.send(false);
            } else {
                return res.send(true);
            }

        }).catch(err => res.send(err))
    };