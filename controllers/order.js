const Order = require('../models/Order');
const User = require('../models/User');
const Product = require('../models/Product');



// [ Create Order Route] User Only
    module.exports.createOrder = async (req, res) => {
    try {
      const { productId, quantity } = req.body;
      const userId = req.user.id;

      // const user = await User.findById(userId);
      if (req.user.isAdmin) {
        return res.status(403).json(false);
      }

      const product = await Product.findById(productId);
      if (!product) {
        return res.status(404).json(false);
      }

      const price = product.price;
      const totalAmount = price * quantity;

      const order = new Order({
        userId,
        products: [{ productId, quantity, price }],
        totalAmount
      });

      await order.save();

      return res.json(true);
    } catch (error) {
      console.error(error);
      return res.status(500).json({ message: err.message });
    }
  };

// [ RETRIEVE ALL Orders for a Specific User ] 
  exports.getUserOrders = async (req, res) => {

    try {
      const { userId } = req.params;

      const orders = await Order.find({userId });
      res.send(orders);
    } catch (error) {
      res.status(500).send({ message: error.message });
    }

  };
