const User = require("../models/User");
const Product = require("../models/Product");
const Order = require('../models/Order');
const bcrypt = require('bcrypt');
const auth = require('../auth');



// [ CHECK User Email Route ]
    module.exports.checkEmailExists = (reqBody) => {
        return User.find({email: reqBody.email}).then(result => {

            if(result.length > 0) {
                return true;
            } else {
                return false;
            }
        })
    };

// [ REGISTER User Route ]
    module.exports.registerUser = (reqBody) => {

        let newUser = new User({

            firstName: reqBody.firstName,
            lastName: reqBody.lastName,
            email: reqBody.email,
            mobileNo: reqBody.mobileNo,
            password: bcrypt.hashSync(reqBody.password, 10)
        })

        return newUser.save().then((user, error) => {

            if(error) {
                return false;
            } else {
                return true;
            }

        }).catch(err => err);
    };

// [ LOGIN User Route ]
    module.exports.loginUser = (req, res) => {
    	return User.findOne({email: req.body.email}).then(result => {

    		if(result == null) {
    			return false
    		} else {
    			const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);
    			if(isPasswordCorrect) {
    				return res.send({access: auth.createAccessToken(result)})
    			} else {
    				return res.send(false);
    			}
    		}
    	}).catch(err => res.send(err));
    };

// [ RETRIEVE User Details Route ]
    module.exports.getProfile = (req, res) => {
        return User.findById(req.user.id).then(result => {  
                result.password = ""; 
                return res.send(result);
            }).catch(err => res.send(err));
    };

// [ UPDATE User Details Route ]
    module.exports.updateUser = (req, res) => {
      const { firstName, lastName, mobileNo } = req.body;

      User.findByIdAndUpdate(req.user.id, { firstName, lastName, mobileNo }, { new: true })
        .then(updatedUser => {
          if (!updatedUser) {
            return res.status(404).json({ message: 'User not found' });
          }

          updatedUser.password = "";
          return res.json(updatedUser);
        })
        .catch(err => {
          return res.status(500).json({ error: err.message });
        });
    };



// [STRETCH GOAL] ADD TO CART
    let cart = {}; // Temporary cart storage
    module.exports.addToCart = async (req, res) => {
      const { productId, quantity } = req.body;

      try {
        const product = await Product.findById(productId);

        if (!product) {
          return res.status(404).json(false);
        }

        const userId = req.user.id;

        if (!cart[userId]) {
          cart[userId] = {};
        }

        if (cart[userId][productId]) {
          cart[userId][productId].quantity += quantity;
        } else {
          cart[userId][productId] = {
            product,
            quantity
          };
        }

        return res.send(true);
      } catch (err) {
        return res.send({ message: err.message });
      }
    };





// [STRETCH GOAL] GET CART ITEMS
    module.exports.getCartItems = async (req, res) => {
      try {
        const userId = req.user.id;

        if (!cart[userId]) {
          return res.send({ cartItems: [] });
        }

        const cartItems = Object.values(cart[userId]).map(item => ({
          productId: item.product._id,
          productName: item.product.name,
          quantity: item.quantity,
          price: item.product.price,
          subtotal: item.quantity * item.product.price
        }));

        return res.send({ cartItems });
      } catch (err) {
        return res.send({ message: err.message });
      }
    };