// [Dependencies and Modules]
const express = require('express');
const userController = require('../controllers/user');
const auth = require('../auth');

const { verify, verifyAdmin } = auth;

const router = express.Router();




// [ CHECK User Email Route ]
	router.post("/checkEmail", (req, res) => {
		userController.checkEmailExists(req.body)
		.then(resultFromController => res.send(resultFromController));
	});

// [ REGISTER User Route ]
	router.post("/register", (req, res) => {
		userController.registerUser(req.body)
		.then(resultFromController => res.send(resultFromController));
	});

// [ LOGIN User Route ]
	router.post("/login", userController.loginUser);

// [ RETRIEVE User Details Route ]
	router.get("/details", verify, userController.getProfile);

// [ UPDATE User Details Route ]
router.put("/update", verify, userController.updateUser);

// [ ADD TO CART ]
router.post("/cart/add", verify, userController.addToCart);

// [STRETCH GOAL] GET CART ITEMS
router.get("/cart/items", verify, userController.getCartItems);

	


module.exports = router;