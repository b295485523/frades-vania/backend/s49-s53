const express = require('express');
const router = express.Router();
const auth = require('../auth');

const orderController = require('../controllers/order');

const { verify, verifyAdmin } = auth;



// [ Create Order Route] User Only
	router.post("/", verify, orderController.createOrder);

// [ RETRIEVE ALL Orders for a Specific User ] 
	router.get("/myorders", verify, orderController.getUserOrders);





module.exports = router;
