const express = require('express');
const productController = require('../controllers/product');
const auth = require('../auth');


const { verify, verifyAdmin } = auth;

const router = express.Router();

// [ ADD Product ] Admin only
	router.post("/", verify, verifyAdmin, productController.addProduct);
	
// [ RETRIEVE All Products ]
	router.get("/all", productController.getAllProducts);

// [ RETRIEVE All Active Products ]
	router.get("/", productController.getAllActive);

// [ RETRIEVE Specific Product ]
	router.get("/:productId", productController.getSpecificProduct);


// [ UPDATE Product Info ] Admin only
	router.put("/:productId", verify, verifyAdmin, productController.updateProduct);

// [ ARCHIVE Product ] Admin only
	router.put("/:productId/archive", verify, verifyAdmin, productController.archiveProduct);

// [ ACTIVATE Product ] Admin only
	router.put("/:productId/activate", verify, verifyAdmin, productController.activateProduct);


module.exports = router;