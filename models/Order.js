const mongoose = require('mongoose');


const orderSchema = mongoose.Schema({


  user: [
    {
      userId: {
      type: String,
      required: true,
      ref: 'User'
      }
    }
  ],


  products: [
    {
      productId: {
        type: String,
        required: true,
        ref: 'Product',
      },
      quantity: {
        type: Number,
        required: true,
        default: 1,
      }
    }
  ],


  totalAmount: {
    type: Number,
    required: true
  },


  purhcasedOn: {
    type: Date,
    default: new Date()
  }


});



module.exports = mongoose.model('Order', orderSchema);
