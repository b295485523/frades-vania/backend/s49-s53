// [ Server Dependencies]
	const express = require('express');
	const mongoose = require('mongoose');
	const cors = require('cors');
	const morgan = require('morgan');

// [ MODEL ./routes ]
	const userRoutes = require("./routes/user");
	const productRoutes = require("./routes/product");
	const orderRoutes = require("./routes/order");

// [Server Setup]
const app = express();

// [Environment Setup]
const port = 4000;

// [Middlewares]
	app.use(express.json());
	app.use(express.urlencoded({extended:true}));
	app.use(morgan('dev')); // Debug App
	app.use(cors()); // Frontend App

// [Mongoose Connection]
	mongoose.connect("mongodb+srv://vaniawilla:admin123@batch295.1amhouq.mongodb.net/e-commerce-API?retryWrites=true&w=majority", 
			{
				useNewUrlParser: true,
				useUnifiedTopology: true
			}
		);


	let db = mongoose.connection;

	db.on('error', console.error.bind(console, 'Connection error'));
	db.once('open', () => console.log('Connected to MongoDB Atlas.'));


	// Backend Routes - http://localhost:3000/
	app.use("/users", userRoutes);
	app.use("/products", productRoutes);
	app.use("/orders", orderRoutes);


	// [Server Gateway Response]
	if(require.main === module) {
		app.listen(process.env.PORT || port, () => { 
		console.log(`Server is now running in port ${process.env.PORT || port}.`)});
	};


	module.exports = {app, mongoose};